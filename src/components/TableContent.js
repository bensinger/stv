import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';
import ProgrammeTable from './ProgrammeTable';
import './TableContent.css';

class TableContent extends Component {

    render() {
        return (
          <Table celled definition>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell />
                <Table.HeaderCell className='hoverable' onClick={() => this.props.sortByAttribute('id')}>ID</Table.HeaderCell>
                <Table.HeaderCell className='hoverable' onClick={() => this.props.sortByAttribute('name')}>Name</Table.HeaderCell>
                <Table.HeaderCell className='hoverable' onClick={() => this.props.sortByAttribute('shortDescription')}>Description</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell />
              </Table.Row>
            </Table.Header>

            <Table.Body>

              {this.props.programmes.map(programme => <ProgrammeTable
                key={programme.id} 
                index={programme.id}
                details={programme}
                removeProgramme={() => this.props.removeProgramme(programme)}
                updateProgramme={() => this.props.updateProgramme(programme)}
                editProgramme={() => this.props.editProgramme(programme)}/>
              )}

            </Table.Body>

          </Table>
        );
    }
}

TableContent.defaultProps = {
  programmes: {}
};

TableContent.propTypes = {
  programmes: PropTypes.array,
  sortByAttribute: PropTypes.func.isRequired,
  removeProgramme: PropTypes.func.isRequired,
  updateProgramme: PropTypes.func.isRequired,
};

export default TableContent;
