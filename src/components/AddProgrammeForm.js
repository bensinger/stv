import React, { Component } from 'react';
import './AddProgrammeForm.css';

class AddProgrammeForm extends Component {
	
	idRef = React.createRef();
	nameRef = React.createRef();
	descRef = React.createRef();
	statusRef = React.createRef();

	createProgramme = event => {
		event.preventDefault();

		if (isNaN(this.idRef.current.value)) {
			window.alert('id must only contain numbers');
			return;
		}
		
		const programme = {
			id: parseFloat(this.idRef.current.value),
			name: this.nameRef.current.value,
			shortDescription: this.descRef.current.value,
			active: JSON.parse(this.statusRef.current.value.toLowerCase()), // !! cast to boolean
		}

		this.props.addProgramme(programme);

		event.currentTarget.reset();
	}

    render() {
        return (
			<form className="ui form form form--create-programme" onSubmit={this.createProgramme}>
				<h3>Add New Programme</h3>
				<div className="equal width fields">
					<div className="field">
						<label htmlFor="name">Programme Name</label>
						<input name="name" ref={this.nameRef} type="text" placeholder="enter programme name" />
					</div>
					<div className="field">
						<label htmlFor="id">Programme ID</label>
						<input name="id" ref={this.idRef} type="text" maxLength="4" placeholder="enter 4 digit programme code" />
					</div>
					<div className="field">
						<label htmlFor="status">Programme Status</label>
						<select name="status" ref={this.statusRef} placeholder="select status">
							<option value={true}>Active</option>
							<option value={false}>Inactive</option>
						</select>
					</div>
				</div>
				<textarea name="desc" ref={this.descRef} placeholder="enter programme description"></textarea>
				<button className="ui button green" type="submit">+ Add Programme</button>
			</form>
        );
    }
}

export default AddProgrammeForm;
