import React, { Component } from 'react';
import './EditProgrammeForm.css';

class EditProgrammeForm extends Component {

	nameRef = React.createRef();
	descRef = React.createRef();
	statusRef = React.createRef();

	saveTheProgramme = event => {
		event.preventDefault();

		const programme = {
			id: this.props.programme.id,
			name: this.nameRef.current.value,
			shortDescription: this.descRef.current.value,
			active: JSON.parse(this.statusRef.current.value.toLowerCase()), // convert value from string to boolean
		}

		this.props.saveProgramme(programme);
	}

    render() {
    	const {id, name, shortDescription, active} = this.props.programme;
        return (
			<form className="ui form form form--create-programme" onSubmit={this.saveTheProgramme}>
				<h3>Edit Programme</h3>
				<div className="equal width fields">
					<div className="field">
						<label htmlFor="name">Programme Name</label>
						<input name="name" defaultValue={name} ref={this.nameRef} type="text" placeholder="edit programme name" />
					</div>
					<div className="field">
						<label htmlFor="status">Programme Status</label>
						<select name="status" defaultValue={active} ref={this.statusRef} placeholder="edit status">
							<option value={true}>Active</option>
							<option value={false}>Inactive</option>
						</select>
					</div>
				</div>
				<textarea name="shortDescription" defaultValue={shortDescription} ref={this.descRef} placeholder="edit programme description"></textarea>
				<button className="ui button green" type="submit">Save Edit</button>
			</form>
        );
    }
}

export default EditProgrammeForm;
