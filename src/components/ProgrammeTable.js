import React, { Component } from 'react';
import './ProgrammeTable.css';
import { Table, Checkbox, Icon, Button } from 'semantic-ui-react';

class ProgrammeTable extends Component {

  	handleRemove = () => {
  		this.props.removeProgramme(this.props.index)
  	}

    handleChange = event => {
        const update = event.target.value;
        console.log(update);
    }

    handleStatus = () => {
      this.props.updateProgramme(this.props.index)
    }

    render() {
    	const { id, name, shortDescription, active } = this.props.details;
    	const isActive = active === true;
        return (
            <Table.Row>
              <Table.Cell collapsing>
                <Checkbox slider defaultChecked={isActive} onClick={this.handleStatus} />
              </Table.Cell>
              <Table.Cell disabled={!isActive}>{id}</Table.Cell>
              <Table.Cell disabled={!isActive}>{name}</Table.Cell>
              <Table.Cell disabled={!isActive}>{shortDescription}</Table.Cell>
              <Table.Cell disabled={!isActive}>{active === true ? 'Active' : 'Inactive'}</Table.Cell>
              <Table.Cell>
                <Button onClick={() => this.props.editProgramme(this.props.details) } icon color='green' size='medium'>
                  <Icon name='edit' />
                </Button>
                <Button onClick={this.handleRemove} icon color='red' size='medium'>
                  <Icon name='times' />
                </Button>
              </Table.Cell>
            </Table.Row>
        );
    }
}

export default ProgrammeTable;
