import React, { Component } from 'react';
import TableContent from './components/TableContent';
import AddProgrammeForm from './components/AddProgrammeForm';
import EditProgrammeForm from './components/EditProgrammeForm';
import programmeData from './data/programmes';
import { Container } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import './App.css';

class App extends Component {

  state = {
    programmes: {},
    sortBy: 'id',
    sortReverse: false,
    searchTerm: '',
    programmeToEdit: null
  };

  searchRef = React.createRef();

  loadProgrammes = () => {
    // get any existing state data from localstorage
    const listings = localStorage.getItem('programmeListings');
    // if state data exists, use that to populate programmes state
    if (listings) {
      this.setState({ programmes: JSON.parse(listings) })
    }
    // if no state data exists, use the original JSON file
    if (!listings) {
        this.setState({ programmes: programmeData.results })
    }
  }

  sortData = () => {
    const { sortBy, sortReverse, searchTerm, programmes } = this.state;
    let sortedProgs = programmes;
    if(searchTerm !== ''){
      sortedProgs = sortedProgs.filter(prog => prog.name.toLowerCase().includes(searchTerm.toLowerCase()));
    }
    sortedProgs = sortedProgs.sort(function(a,b) {return (a[sortBy] > b[sortBy]) ? 1 : ((b[sortBy] > a[sortBy]) ? -1 : 0);} );
    return sortReverse ? sortedProgs.reverse() : sortedProgs;
  }

  componentWillMount() {
    // load our state data
    this.loadProgrammes()
  }

  sortByAttribute = sortByAttr => {
    this.setState(prevState => ({
      sortBy: sortByAttr,
      sortReverse:
        prevState.sortBy === sortByAttr ? !prevState.sortReverse : false,
    }));
  };

  showFilteredProgrammes = event => {
    const search = this.searchRef.current.value;
    this.setState({ searchTerm: search });
  }

  componentDidUpdate() {
    // save the programme state data in localstorage if the app updates
    localStorage.setItem(
      'programmeListings',
      JSON.stringify(this.state.programmes)
    )
  }

  addProgramme = programme => {
    const programmes = this.state.programmes;
    programmes.push(programme);
    this.setState({programmes});
  }

  removeProgramme = item => {
    let programmes = this.state.programmes; 
    programmes.splice(programmes.findIndex(x => x.id === item.id), 1);
    this.setState({ programmes });
  }

  toggleActiveSlider = item => {
    let programmes = this.state.programmes;
    const selectedItem = programmes.find(x => x.id === item.id);
    // check active status of program and alter it on slider movement
    selectedItem.active = !selectedItem.active;
    
    // set this change to state
    this.setState({ programmes });
  }

  handleEditView = item => {
    let programmes = this.state.programmes;
    const selectedItem = programmes.find(x => x.id === item.id);
    this.setState({ programmeToEdit: selectedItem });
  }

  saveProgramme = item => {
    let programmes = this.state.programmes;
    const selectedItem = programmes.find(x => x.id === item.id);

    selectedItem.name = item.name;
    selectedItem.shortDescription = item.shortDescription;
    selectedItem.active = item.active;
    
    // set this change to state
    this.setState({ programmes, programmeToEdit: null });
  }

  tableView(sortedData) {
    return (
      <div>
        <h3>Filter Programmes {this.state.searchTerm}</h3>
        <div className="ui input">
          <input ref={this.searchRef} placeholder="filter programme list" onChange={this.showFilteredProgrammes} type="text" className="input input--search ui"/>
        </div>
        
        <TableContent sortByAttribute={this.sortByAttribute} programmes={sortedData} updateProgramme={this.toggleActiveSlider} editProgramme={this.handleEditView} removeProgramme={this.removeProgramme} />

        <AddProgrammeForm addProgramme={this.addProgramme} />
      </div>
    )
  }

  editView() {
    return (
      <div>
        <h3>Edit {this.state.programmeToEdit.name}</h3>
        <EditProgrammeForm programme={this.state.programmeToEdit} saveProgramme={this.saveProgramme} />
      </div>
    )
  }

  render() {
    const sortedData = this.sortData();
    return (
      <Container>
        <div className="App">

          <header className="App-header">
            <h1 className="App-title">STV React Test Application</h1>
          </header>

          {this.state.programmeToEdit ? this.editView() : this.tableView(sortedData)}

        </div>
      </Container>
    );
  }
}

export default App;